/**
 * Created by yuriy on 02.11.14.
 */

var socketio =require('socket.io');
var io;
var guestNumber = 1;
var nickNames = {};
var namesUsed = [];
var currentRoom = {};

//Запуск SocketIO-сервера
exports.listen = function (server) {
    //Запуск SocketIO-сервера, чтобы выполняться вместе с существующим HTTP-сервером
    io = socketio.listen(server);
    io.set('log level', 1);
    //Определение способа обработки каждого пользовательского соединения
    io.sockets.on('connection', function (socket) {
        guestNumber = assignGuestName(socket, guestNumber, nickNames, namesUsed);   //Присваивание подкл. пользователю
                                                                                    // имени guest
        joinRoom(socket, 'Lobby');  //Помещение подкл-ся поль-ля в комнату 'Lobby'

        handleMessageBroadcasting(socket, nickNames); //Обработка пользовательских сообщений
        handleNameChangeAttempts(socket, nickNames, namesUsed); //Обработка попыток изменения имени
        handleRoomJoining(socket);  //Обработка попыток создания/изменения комнат

        // Вывод списка занятых комнат по запросу пользователя
        socket.on('rooms', function () {
            socket.emit('rooms', io.sockets.manager.rooms);
        });

        //Определение логики очистки, выполняемой после выхода пользователя из чата
        handleClientDisconnection(socket, nickNames, namesUsed);
    });
};

var assignGuestName = function (socket, guestNumber, nickNames, namesUsed) {
    //Создание нового гостевого имени
    var name = 'Guest'+guestNumber;

    //Связывание гост-го имени с идентификатором клиентского подключения
    nickNames[socket.id] = name;

    //Сообщение польз-лю его гостевого имени
    socket.emit('nameResult', {
        success: true,
        name: name
    });

    namesUsed.push(name);

    //Генерация гостевых имен
    return guestNumber + 1;
};

var joinRoom = function (socket, room) {
    console.log('joinRoom');
    //Вход пользователя в комнату чата
    socket.join(room);

    //Обнаружение поль-ля в данной комнате
    currentRoom[socket.id] = room;

    //Оповещение п. о том, что он находится в новой комнате
    socket.emit('joinResult', {room: room});

    //Оповещение других пользователей о появлении нового пользователя в комнате чата
    socket.broadcast.to(room).emit('message', {
        text: nickNames[socket.id] + 'has joined' + room + '.'
    });

    //Идентификация др. ползователей, нах-ся в той же комнате, что и поль-ль
    var usersInRoom = io.sockets.clients(room);
    //Суммирование др. пользователей
    if(usersInRoom.length > 1){
        var usersInRoomSummary = 'Users currently in '+ room + ':';

        for(var index in usersInRoom){
            var userSocketId = usersInRoom[index].id;
            if(userSocketId != socket.id){
                if(index > 0){
                    usersInRoomSummary += ',';
                }
                usersInRoomSummary += nickNames[userSocketId];
            }
        }
        usersInRoomSummary += '.';

        //Вывод отчет ао др. пользователях наход-ся в комнате
        socket.emit('message', {text: usersInRoomSummary});
    }
};

//Запрос об изменении имени
var handleNameChangeAttempts = function (socket, nickNames, namesUsed) {
    console.log('handleNameChangeAttempts ');
    //Добавление слушателя событий nameAttempt
    socket.on('nameAttempt', function (name) {
        console.log('nameAttempt', name);
        //Не допускаются имена начин-ся с Guest
        if(name.indexOf('Guest') == 0){
            console.log('guest');
            socket.emit('nameResult', {
                success: false,
                message: 'Names cannot begin with "Guest".'
            });
        } else {
            console.log(2);
            // Если имя не используется, выбирете его
            if(namesUsed.indexOf(name) == -1){
                var previousName = nickNames[socket.id];
                var previousNameIndex = namesUsed.indexOf(previousName);
                namesUsed.push(name);
                nickNames[socket.id] = name;

                //Удаление ранее выбранного имени, кот. освобождается для
                // других клиентов
                delete namesUsed[previousNameIndex];
                socket.emit('nameResult', {
                    success: true,
                    name: name
                });
                socket.broadcast.to(currentRoom[socket.id]).emit('message',{
                    text: previousName + 'is now known as '+ name + '.'
                });
            } else {
                socket.emit('nameResult', {
                    //Если имя зарег-но, то отправить клиенту сообщение об ошибке
                    success: false,
                    massage: 'That name is already in use.'
                });
            }
        }
    });
};

//Передача сообщения в чате
var handleMessageBroadcasting = function (socket) {
    socket.on('message', function (message) {
        socket.broadcast.to(message.room).emit('message', {
            text: nickNames[socket.id] + ': ' + message.text
        });
    });
};

//Переход в другую комнату чата
var handleRoomJoining = function (socket) {
    socket.on('join', function (room) {
        socket.leave(currentRoom[socket.id]);
        joinRoom(socket, room.newRoom);
    });
};

//Обработка выхода пользователя из чата
var handleClientDisconnection = function (socket) {
    socket.on('disconnect', function () {
        var nameIndex = namesUsed.indexOf(nickNames[socket.id]);
        delete namesUsed[nameIndex];
        delete nickNames[socket.id];
    });
};