/**
 * Created by yuriy on 01.11.14.
 */

//Объявление переменных
var http = require('http');
var fs = require('fs');
var path = require('path'); //функ-ть связанная с путями файловой системы
var mime = require('mime');// поддерживает порождение MIME-типов на основе расширения имен файлов
var cache = {}; //хранение содержимого кешированных файлов

//404 - если файл отсутствует
var send404 = function (response){
    response.writeHead(404, {'Content-Type': 'text/plain'});
    response.write('404: Page not found');
    response.end();
};

//200 - успех
var sendFile = function (response, filePath, fileContents) {
    response.writeHead(200, {'Content-Type': mime.lookup(path.basename(filePath))});
    response.end(fileContents);
};

//использование статических файлов
var serverStatic = function (response, cache, absPath) {
    //кэширован ли файл
    if(cache[absPath]){
        //если файл в памяти
        sendFile(response, absPath, cache[absPath]);
    } else {
        //проверка на существование файла
        fs.exists(absPath, function (exists) {
            if(exists){
                //Считывание файла с диска
                fs.readFile(absPath, function (err, data) {
                    if(err){
                        send404(response);
                    } else {
                        cache[absPath] = data;
                        sendFile(response, absPath, data);
                    }
                });
            } else {
                send404(response);
            }
        });
    }
};

//HTTP-сервер
var server = http.createServer(function (req, res) {
    var filePath = false;
    if(req.url == '/'){
        filePath = 'public/index.html';
    } else {
        //Преобразование URL-адреса в относительный путь к файлу
        filePath = 'public' + req.url;
    }

    var absPath = './' + filePath;

    //Обслуживание статического файла
    serverStatic(res, cache, absPath);
});

server.listen(process.env.PORT || 3003, function () {
    console.log("Server is running on port 3003");
});

var chatServer = require('./lib/chart_server');
chatServer.listen(server);